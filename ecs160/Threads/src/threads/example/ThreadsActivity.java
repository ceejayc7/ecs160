package threads.example;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ThreadsActivity extends Activity {
	private long SLEEP = 10 * 1000;
	
	private ProgressDialog pd;
	private AsyncBlock task = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button b = (Button) findViewById(R.id.button);
        b.setOnClickListener(new OnClickListener() {
			//@Override
			public void onClick(View v) {
				
				// Blocking action on UI thread
				blockingAction();
				
				// Use the AsyncTask class
				task = new AsyncBlock();
				task.execute();
			}
		});
        
        b = (Button) findViewById(R.id.hi);
        b.setOnClickListener(new OnClickListener() {
			
			//@Override
			public void onClick(View v) {
				sayHi();
			}
		});
    }
    
    @Override
    protected void onPause() {
    	// This line ensures the AsyncTask is stopped when the activity is no longer the displayed
    	// activity.
    	if (task != null) task.cancel(true);
    	if (pd != null) pd.cancel();
    	super.onPause();
    }
    
    private void blockingAction() {
    	showProgress();
    	
    	try {
			Thread.sleep(SLEEP);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	notifyFinish();
    }
    
    private class AsyncBlock extends AsyncTask<Void, Void, Void> {
    	
    	@Override
    	protected void onPreExecute() {
    		super.onPreExecute();
    		showProgress();
    	}

		@Override
		protected Void doInBackground(Void...params) {
			
			try {
				Thread.sleep(SLEEP);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!isCancelled())
				notifyFinish();
		}		
    }
    
    private void notifyFinish() {
    	if (pd != null) pd.dismiss();
    	Toast.makeText(this, "Done!", Toast.LENGTH_LONG).show();
    }
    
    private void sayHi() {
    	Toast.makeText(this, "Hi!", Toast.LENGTH_SHORT).show();
    }
    
    private void showProgress() {
		
		// This will show a progress dialog on the screen
		pd = new ProgressDialog(this);
		pd.setTitle("");
		pd.setMessage("Loading...");
		pd.setButton("Cancel", new DialogInterface.OnClickListener() {
			
			//@Override
			public void onClick(DialogInterface dialog, int which) {
				pd.cancel();
			}
		});
		pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			//@Override
			public void onCancel(DialogInterface dialog) {
				if (task != null) task.cancel(true);
				Toast.makeText(ThreadsActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
			}
		});
		pd.show();
    }
}