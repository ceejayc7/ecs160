package com.StudyMob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.StudyMob.HomeActivity.home_activity_http;

import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewDebug.IntToString;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CreateGroupActivity extends Activity implements OnClickListener {
	public static final String TOKEN_PREFS = "tokens";
	Button submit;
	Button setClass;
	String username;
	String password;
	String course;
	String starttime;
	String description;
	String location;
	String courseName = "";
	int classID = -1;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creategroup);
	    
        SharedPreferences settings = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0);
        username = settings.getString("username","");
        password = settings.getString("password","");
        
		 Editor tokenFile = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0).edit();
		 tokenFile.putString("courseName", "");
		 tokenFile.putInt("courseId", -1);
		 tokenFile.commit();
        
        submit = (Button)findViewById( R.id.submit );
        submit.setOnClickListener(this);
        setClass = (Button)findViewById( R.id.setclass );
        setClass.setOnClickListener(this);
    }
	
	protected void onResume()
	{
		super.onResume();
		SharedPreferences settings = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0);
        courseName = settings.getString("courseName","");
        classID = settings.getInt("courseId",-1); 
        TextView course = (TextView) findViewById(R.id.selectedclass);
        course.setText("Selected Class/Course: " + courseName);  
	}
	
	public void onClick(View v)
    {
    	if(v.equals(findViewById(R.id.submit)))
    	{	
    		if(classID != -1) {
    			EditText pin1 = (EditText) findViewById(R.id.starttime);
    			starttime = pin1.getText().toString().trim();
    			pin1.setText("");
			
    			EditText pin2 = (EditText) findViewById(R.id.description);
    			description = pin2.getText().toString().trim();
    			pin2.setText("");
			
    			EditText pin3 = (EditText) findViewById(R.id.location);
    			location = pin3.getText().toString().trim();
    			pin3.setText("");
			
    			new http_push().execute(this);
    			//http_push();
			
    			// Close virtual keyboard
    			InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
    			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			
    			//finish();
    		}
    		else {
    			AlertDialog alertdialog = new AlertDialog.Builder(this).create();
                alertdialog.setTitle("Error:");
                alertdialog.setMessage("You must select a Class/Course");
                alertdialog.setButton("Close", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which) { return;}
                });
                alertdialog.show();
    		}
    	}
    	
    	if(v.equals(findViewById(R.id.setclass)))
    	{	
    		Intent i = new Intent(this, SelectCourseActivity.class);
			startActivity(i);
    	}
    }
	
	/*
	 @Override
	 protected void onListItemClick(ListView l, View v, int position, long id) {
		 super.onListItemClick(l, v, position, id);
		 System.out.println("test on click listener");
		 String course = (String) l.getAdapter().getItem(position);
	 }
	 */

	

	
	
	
	
	
	//void ();
	
	
	 private class http_push extends
		AsyncTask<CreateGroupActivity, Void, CreateGroupActivity>
{
			//String[] TempDisplay;
			//int[] TempDisplayIDS;
		 boolean problem;

	@Override
	protected void onPreExecute()
	{
		problem = false;
	}

	protected CreateGroupActivity doInBackground( CreateGroupActivity... params)
	{
		HttpClient httpclient1 = new DefaultHttpClient();
        HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/newmob.php");
        //InputStream is = null;
        //String result = "";
        try {
        	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
        	nameValuePairs.add(new BasicNameValuePair("op","andlogin"));
        	nameValuePairs.add(new BasicNameValuePair("email", username));
        	nameValuePairs.add(new BasicNameValuePair("password", password));
        	nameValuePairs.add(new BasicNameValuePair("classid", Integer.toString(classID)));
        	nameValuePairs.add(new BasicNameValuePair("starttime", starttime));
        	nameValuePairs.add(new BasicNameValuePair("description", description));
        	nameValuePairs.add(new BasicNameValuePair("location", location));
        	httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        
        	//HttpResponse response = httpclient1.execute(httppost1);
        	httpclient1.execute(httppost1);
        	//HttpEntity entity = response.getEntity();
            //is = entity.getContent();
        	//HttpEntity entity = response.getEntity();
        }
    	catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		problem = true;
    		e.printStackTrace();
    	} 
    	catch (IOException e) {
    		// TODO Auto-generated catch block
    		problem = true;
    		e.printStackTrace();
    	}
			return params[0];
	}

	protected void onPostExecute(CreateGroupActivity result)
	{
		//result.Display = this.TempDisplay;//new String[TempDisplay.length];
		//result.DisplayIDS = this.TempDisplayIDS;//new String[TempDisplayIDS.length];
		//result.updateUI();
		//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));  
		//HomeActivity
		if(!problem)
			Toast.makeText(result, "Mob Created", 200);
		else
			Toast.makeText(result, "Issues with http connection\n mob most likely not created", 200);
		result.finish();
	}
		

	}
	 
	
	
	
	
	
}