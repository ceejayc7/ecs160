package com.StudyMob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug.IntToString;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class homescreen extends ListActivity
{
	String title[];
	int courseID[];
	String Display[];
	char stat[];
	String username;
	String password;

	int http_push_cid;
	char http_push_value;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homescreen);
		SharedPreferences settings = getSharedPreferences(
				StudyMobActivity.TOKEN_PREFS, 0);
		username = settings.getString("username", "");
		password = settings.getString("password", "");
		/*
		 * new Thread(new Runnable() { public void run() { http_connection(); }
		 * }).start();
		 */
		title = new String[0];
		Display = new String[0];

		new http_connection().execute(this);

		updateUI();

	}

	void updateUI()
	{
		Display = new String[title.length];
		for (int i = 0; i < Display.length; i++)
		{
			if (stat[i] == 'c')
			{
				Display[i] = title[i] + " (need help)";
			} else if (stat[i] == 'p')
			{
				Display[i] = title[i] + " (can help)";
			} else
			{
				Display[i] = title[i] + " (neutral)";
			}
		}
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, Display));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		super.onListItemClick(l, v, position, id);
		System.out.println("test on click listener");
		String course = (String) l.getAdapter().getItem(position);
		Display = new String[title.length];

		if (stat[position] == 'c')
		{
			stat[position] = 'p';
		} else if (stat[position] == 'p')
		{
			stat[position] = 'n';
		} else
		{
			stat[position] = 'c';
		}

		http_push_value = stat[position];
		http_push_cid = courseID[position];
		new http_push().execute(this);
	}

	private class http_push extends AsyncTask<homescreen, Void, homescreen>
	{
		// String[] TempDisplay;
		// int[] TempDisplayIDS;
		// boolean problem;

		@Override
		protected void onPreExecute()
		{
			// problem = false;
		}

		protected homescreen doInBackground(homescreen... params)
		{
			HttpClient httpclient1 = new DefaultHttpClient();
			HttpPost httppost1 = new HttpPost(
					"http://www.studymob.comuv.com/modc.php");
			// InputStream is = null;
			// String result = "";
			homescreen mainui = params[0];
			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						5);
				nameValuePairs.add(new BasicNameValuePair("op", "andlogin"));
				nameValuePairs.add(new BasicNameValuePair("email", username));
				nameValuePairs
						.add(new BasicNameValuePair("password", password));
				nameValuePairs.add(new BasicNameValuePair("cid", Integer
						.toString(mainui.http_push_cid)));
				Log.v("http_push cid to string",
						Integer.toString(mainui.http_push_cid));
				nameValuePairs.add(new BasicNameValuePair("mval", Character
						.toString(mainui.http_push_value)));
				Log.v("http_push char to string",
						Character.toString(mainui.http_push_value));
				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				httpclient1.execute(httppost1);
				// HttpEntity entity = response.getEntity();
				// is = entity.getContent();
			} catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return params[0];
		}

		protected void onPostExecute(homescreen result)
		{
			result.updateUI();
		}

	}

	private class http_connection extends
			AsyncTask<homescreen, Void, homescreen>
	{
		// String[] TempDisplay;
		// int[] TempDisplayIDS;
		boolean problem;

		@Override
		protected void onPreExecute()
		{
			problem = false;
		}

		protected homescreen doInBackground(homescreen... params)
		{
			HttpClient httpclient1 = new DefaultHttpClient();
			HttpPost httppost1 = new HttpPost(
					"http://www.studymob.comuv.com/getclasses.php");
			InputStream is = null;
			String result = "";
			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						3);
				nameValuePairs.add(new BasicNameValuePair("op", "andlogin"));
				nameValuePairs.add(new BasicNameValuePair("email", username));
				nameValuePairs
						.add(new BasicNameValuePair("password", password));
				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				HttpResponse response = httpclient1.execute(httppost1);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			} catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			StringBuilder sb = new StringBuilder();
			// convert response to string
			try
			{
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "UTF-8"));/* "iso-8859-1"),8); */
				String line = null;
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
			} catch (Exception e)
			{
				Log.e("Connecting", "Error converting result " + e.toString());
			}

			// parse json data
			try
			{

				Log.v("homescreen", result);
				JSONArray jArray = new JSONArray(result);
				// Log.v("test json",jObject.getString("logged"));
				Display = new String[jArray.length()];
				stat = new char[jArray.length()];
				title = new String[jArray.length()];
				courseID = new int[jArray.length()];

				for (int i = 0; i < jArray.length(); i++)
				{
					Log.v("array", Integer.toString(jArray.length()));
					// JSONObject j = jArray.getJSONObject(Integer.toString(i));
					// JSONObject j = jArray.getJSONObject(Integer.toString(i));
					JSONObject j = jArray.getJSONObject(i);
					Log.v("debug", "debug 0");
					// "dbcid" "dbtitle" "cstat"
					courseID[i] = j.getInt("dbcid");
					Log.i("courseid", Integer.toString(courseID[i]));
					Log.v("debug", "debug .5");
					title[i] = j.getString("dbtitle");
					Log.v("debug", "debug 1");
					stat[i] = j.getString("cstat").charAt(0);
					Log.v("debug", "debug 2");

				}

				// System.out.println(jObject.getString("logged"));

			} catch (JSONException e)
			{
				Log.e("Parsing", "Error parsing data " + e.toString());
			}
			return params[0];
		}

		protected void onPostExecute(homescreen result)
		{
			result.updateUI();
		}

	}

}
