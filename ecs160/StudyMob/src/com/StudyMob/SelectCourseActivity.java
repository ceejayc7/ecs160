package com.StudyMob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug.IntToString;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class SelectCourseActivity extends ListActivity {
	String title[] ;//= {"ECS 10", "ECS 20", "ECS 30", "ECS 40", "ECS 50", "ECS 60"};
	int courseID[];
	String Display[];
	char stat[] ;//= {'n','n','n','n','n','n'};
	String username;
	String password;
	
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.homescreen);
        SharedPreferences settings = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0);
        username = settings.getString("username","");
        password = settings.getString("password","");
        
        Display = new String[0];
        title = new String[0];
        stat = new char[0];
        courseID = new int[0];
        new http_connection().execute(this);
        updateUI();

    }
	
	
	 @Override
	 protected void onListItemClick(ListView l, View v, int position, long id) {
		 super.onListItemClick(l, v, position, id);
		 
		 Editor tokenFile = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0).edit();
		 tokenFile.putString("courseName", title[position]);
		 tokenFile.putInt("courseId", courseID[position]);
		 tokenFile.commit();
		 finish();
	 }
	 
	 
	 void updateUI()
	 {
	        
	        Display = new String[title.length];
			 for(int i = 0; i < Display.length ; i++)
			 {
				 Display[i] = title[i];
			 }
			setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));
		 
	 }
	 

    private class http_connection extends
	AsyncTask<SelectCourseActivity, Void, SelectCourseActivity>
{

@Override
protected void onPreExecute()
{

}

protected SelectCourseActivity doInBackground(SelectCourseActivity... params)
{
	SelectCourseActivity mainUI = params[0];
	
	HttpClient httpclient1 = new DefaultHttpClient();
    HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/getclasses.php");
    InputStream is = null;
    String result = "";
    try {
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
    	nameValuePairs.add(new BasicNameValuePair("op","andlogin"));
    	nameValuePairs.add(new BasicNameValuePair("email", username));
    	nameValuePairs.add(new BasicNameValuePair("password", password));
    	httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    
    	HttpResponse response = httpclient1.execute(httppost1);
    	HttpEntity entity = response.getEntity();
        is = entity.getContent();
        
    	} 
    	catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} 
    	catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	//convert response to string
    	try{
    	    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));/*"iso-8859-1"),8);*/
    	    String line = null;
    	    while ((line = reader.readLine()) != null) {
    	        sb.append(line + "\n");
    	    }
    	    is.close();
    	    result=sb.toString();
    	}catch(Exception e){
    	    Log.e("Connecting", "Error converting result "+e.toString());
    	}

    	//parse json data
    	try{

    		Log.v("homescreen", result);
    		JSONArray jArray = new JSONArray(result);
    		//Log.v("test json",jObject.getString("logged"));
    		Display = new String[jArray.length()];
    		stat = new char[jArray.length()];
    		title = new String[jArray.length()];
    		courseID = new int[jArray.length()];
    		
    		for(int i = 0 ; i < jArray.length() ; i++)
    		{
    			Log.v("array",Integer.toString(jArray.length()));
    			//JSONObject j = jArray.getJSONObject(Integer.toString(i));
    			//JSONObject j = jArray.getJSONObject(Integer.toString(i));
    			JSONObject j = jArray.getJSONObject(i);
    			Log.v("debug","debug 0");
    			//"dbcid" "dbtitle" "cstat"
    			courseID[i]= j.getInt("dbcid");
    			Log.i("courseid",Integer.toString(courseID[i]));
    			Log.v("debug","debug .5");
    			title[i]=j.getString("dbtitle");
    			Log.v("debug","debug 1");
    			stat[i] = j.getString("cstat").charAt(0);
    			Log.v("debug","debug 2");
    			
    			
    		}

    		
    	   
    	}
    	catch(JSONException e){
    	   Log.e("Parsing", "Error parsing data "+e.toString());
    	}
	
	
	
	return params[0];
}

protected void onPostExecute(SelectCourseActivity result)
{
	result.updateUI();
}

}
    
    
    
    
    
}

