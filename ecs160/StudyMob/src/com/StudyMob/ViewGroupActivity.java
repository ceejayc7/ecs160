package com.StudyMob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ViewGroupActivity extends Activity implements OnClickListener
{
	public static final String TOKEN_PREFS = "tokens";

	int classID;
	String courseString;
	String timeString;
	String descriptionString;
	String locationString;
	String username;
	String password;
	String joinText;
	String email;

	Button join;
	Button send;

	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewgroup);
		SharedPreferences settings = getSharedPreferences(StudyMobActivity.TOKEN_PREFS, 0);
		username = settings.getString("username", "");
		password = settings.getString("password", "");
		classID = settings.getInt("classID", 0);

		join = (Button) findViewById(R.id.join);
		join.setOnClickListener(this);
		join.setVisibility(View.INVISIBLE);
		send = (Button) findViewById(R.id.send);
		send.setOnClickListener(this);
		send.setVisibility(View.INVISIBLE);

		new http_connection().execute(this);
		// http_check();
		// updateUI();
	}

	public void onClick(View v)
	{
		if (v.equals(findViewById(R.id.join)))
		{
			new http_join().execute(this);
			//new http_check().execute(this);
			// Intent i = new Intent(this, HomeActivity.class);
			// startActivity(i);
		}
		if (v.equals(findViewById(R.id.send)))
		{
			EditText pin0 = (EditText) findViewById(R.id.email);
			email = pin0.getText().toString().trim();
			pin0.setText("");
			new http_push().execute(this);
		}
	}

	void updateUI()
	{
		TextView course = (TextView) findViewById(R.id.course);
		course.setText(courseString);
		TextView time = (TextView) findViewById(R.id.time);
		time.setText(timeString);
		TextView description = (TextView) findViewById(R.id.description2);
		description.setText(descriptionString);
		TextView location = (TextView) findViewById(R.id.location1);
		location.setText(locationString);
	}

	private class http_connection extends AsyncTask<ViewGroupActivity, Void, ViewGroupActivity>
	{

		@Override
		protected void onPreExecute()
		{

		}

		protected ViewGroupActivity doInBackground(ViewGroupActivity... params)
		{
			ViewGroupActivity mainUI = params[0];
			HttpClient httpclient1 = new DefaultHttpClient();
			HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/single.php");
			InputStream is = null;
			String result = "";
			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
				nameValuePairs.add(new BasicNameValuePair("op", "andlogin"));
				nameValuePairs.add(new BasicNameValuePair("email", username));
				nameValuePairs.add(new BasicNameValuePair("password", password));
				nameValuePairs.add(new BasicNameValuePair("mobid", Integer.toString(classID)));
				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				HttpResponse response = httpclient1.execute(httppost1);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			} catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			StringBuilder sb = new StringBuilder();
			// convert response to string
			try
			{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				String line = null;
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
			} catch (Exception e)
			{
				Log.e("Connecting", "Error converting result " + e.toString());
			}

			// parse json data
			try
			{
				JSONObject jobject = new JSONObject(result);
				courseString = jobject.getString("classtitle");
				timeString = jobject.getString("start");
				descriptionString = jobject.getString("desc");
				locationString = jobject.getString("loc");
			} catch (JSONException e)
			{
				Log.e("Parsing", "Error parsing data " + e.toString());
			}
			return mainUI;
		}

		protected void onPostExecute(ViewGroupActivity mainUI)
		{
			new http_check().execute(mainUI);
			mainUI.join.setVisibility(View.VISIBLE);
			mainUI.send.setVisibility(View.VISIBLE);
			mainUI.updateUI();
		}

	}

	private class http_push extends AsyncTask<ViewGroupActivity, Void, ViewGroupActivity>
	{

		@Override
		protected void onPreExecute()
		{

		}

		protected ViewGroupActivity doInBackground(ViewGroupActivity... params)
		{
			ViewGroupActivity mainUI = params[0];
			
			HttpClient httpclient1 = new DefaultHttpClient();
			HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/sendinvite.php");
			// InputStream is = null;
			// String result = "";
			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
				nameValuePairs.add(new BasicNameValuePair("op", "andlogin"));
				nameValuePairs.add(new BasicNameValuePair("email", username));
				nameValuePairs.add(new BasicNameValuePair("password", password));
				nameValuePairs.add(new BasicNameValuePair("mid", Integer.toString(classID)));
				nameValuePairs.add(new BasicNameValuePair("invitee", email));
				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				httpclient1.execute(httppost1);
				// HttpEntity entity = response.getEntity();
				// is = entity.getContent();
			} catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return mainUI;
		}

		protected void onPostExecute(ViewGroupActivity mainUI)
		{

		}

	}

	private class http_join extends AsyncTask<ViewGroupActivity, Void, ViewGroupActivity>
	{
			String result = "";
			String status = "";
		@Override
		protected void onPreExecute()
		{

		}

		protected ViewGroupActivity doInBackground(ViewGroupActivity... params)
		{
			ViewGroupActivity mainUI = params[0];
			HttpClient httpclient1 = new DefaultHttpClient();
			HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/joinleave.php");
			InputStream is = null;

			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
				nameValuePairs.add(new BasicNameValuePair("op", "andlogin"));
				nameValuePairs.add(new BasicNameValuePair("email", username));
				nameValuePairs.add(new BasicNameValuePair("password", password));
				nameValuePairs.add(new BasicNameValuePair("mid", Integer.toString(classID)));

				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient1.execute(httppost1);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			} catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			StringBuilder sb = new StringBuilder();
			// convert response to string
			try
			{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				String line = null;
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
			} catch (Exception e)
			{
				Log.e("Connecting", "Error converting result " + e.toString());
			}

			// parse json data
			try
			{
				JSONObject jobject = new JSONObject(result);
				status = jobject.getString("ERROR");
			} catch (JSONException e)
			{
				Log.e("Parsing", "Error parsing data " + e.toString());
			}


			return mainUI;
		}

		protected void onPostExecute(ViewGroupActivity mainUI)
		{
			if (status.equals("true"))
			{
				// joinText = "Leave StudyMob";
				// join.setText(joinText);
				AlertDialog alertdialog = new AlertDialog.Builder(mainUI).create();
				alertdialog.setTitle("Error:");
				alertdialog.setMessage("StudyMob no longer exists");
				// Toast.makeText(ViewGroupActivity.this,"test",Toast.LENGTH_LONG).show();
				alertdialog.setButton("Close", new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
						finish();
						return;
					}
				});

				alertdialog.show();
			} else if (status.equals("false"))
			{
				// joinText = "Join StudyMob";
				// join.setText(joinText);
			}
			new http_check().execute(mainUI);
		}

	}

	private class http_check extends AsyncTask<ViewGroupActivity, Void, ViewGroupActivity>
	{
		String result = "";
		String status = "";

		@Override
		protected void onPreExecute()
		{

		}

		protected ViewGroupActivity doInBackground(ViewGroupActivity... params)
		{
			ViewGroupActivity mainUI = params[0];

			HttpClient httpclient1 = new DefaultHttpClient();
			HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/getjoinstatus.php");
			InputStream is = null;

			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
				nameValuePairs.add(new BasicNameValuePair("op", "andlogin"));
				nameValuePairs.add(new BasicNameValuePair("email", username));
				nameValuePairs.add(new BasicNameValuePair("password", password));
				nameValuePairs.add(new BasicNameValuePair("mid", Integer.toString(classID)));

				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				HttpResponse response = httpclient1.execute(httppost1);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			} catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			StringBuilder sb = new StringBuilder();
			// convert response to string
			try
			{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				String line = null;
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
			} catch (Exception e)
			{
				Log.e("Connecting", "Error converting result " + e.toString());
			}

			// parse json data
			try
			{
				JSONObject jobject = new JSONObject(result);
				status = jobject.getString("status");
			} catch (JSONException e)
			{
				Log.e("Parsing", "Error parsing data " + e.toString());
			}

			return mainUI;
		}

		protected void onPostExecute(ViewGroupActivity mainUI)
		{

			if (status.equals("true"))
			{
				joinText = "Leave StudyMob";
				join.setText(joinText);
			} else if (status.equals("false"))
			{
				joinText = "Join StudyMob";
				join.setText(joinText);
			}
		}

	}

}
