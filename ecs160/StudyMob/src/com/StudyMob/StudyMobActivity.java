package com.StudyMob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StudyMobActivity extends Activity implements OnClickListener
{
	public static final String TOKEN_PREFS = "tokens";
	Button login;
	Button register;
	String username;
	String password;
	boolean userExists = false;
	View tmpv;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		login = (Button) findViewById(R.id.login);
		login.setOnClickListener(this);
		register = (Button) findViewById(R.id.register);
		register.setOnClickListener(this);
	}

	public void onClick(View v)
	{
		if (v.equals(findViewById(R.id.register)))
		{
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.studymob.comuv.com/reg.php"));
			startActivity(i);
		}
		if (v.equals(findViewById(R.id.login)))
		{
			EditText pin1 = (EditText) findViewById(R.id.username);
			username = pin1.getText().toString().trim();
			pin1.setText("");

			EditText pin2 = (EditText) findViewById(R.id.password);
			password = pin2.getText().toString().trim();
			pin2.setText("");
			tmpv = v;
			new http_connection().execute(this);
			// tryLogin("user", "password");


		}
	}

	void updateUI()
	{
		if (userExists)
		{
			Editor tokenFile = getSharedPreferences(TOKEN_PREFS, 0).edit();
			tokenFile.putString("username", username);
			tokenFile.putString("password", password);
			tokenFile.commit();
			// Intent i = new Intent(this, homescreen.class);
			Intent i = new Intent(this, HomeActivity.class);
			Log.v("Create Activity", "Create Activity");

			// Close virtual keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(tmpv.getWindowToken(), 0);

			startActivity(i);
		} else
		{
			AlertDialog alertdialog = new AlertDialog.Builder(this).create();
			alertdialog.setTitle("Error:");
			alertdialog.setMessage("Incorrect Email or Password");
			alertdialog.setButton("Close", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					return;
				}
			});
			alertdialog.show();
		}
	}
	
	private class http_connection extends AsyncTask<StudyMobActivity, Void, StudyMobActivity>
	{

		@Override
		protected void onPreExecute()
		{

		}

		protected StudyMobActivity doInBackground(StudyMobActivity... params)
		{
			StudyMobActivity mainUI = params[0];
			HttpClient httpclient1 = new DefaultHttpClient();
			HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/login.php");
			InputStream is = null;
			String result = "";

			try
			{
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
				nameValuePairs.add(new BasicNameValuePair("op", "andlogin"));
				nameValuePairs.add(new BasicNameValuePair("email", username));
				nameValuePairs.add(new BasicNameValuePair("password", password));
				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				HttpResponse response = httpclient1.execute(httppost1);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
			} catch (ClientProtocolException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			StringBuilder sb = new StringBuilder();
			// convert response to string
			try
			{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

				String line = null;
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				is.close();

				result = sb.toString();
				Log.v("result", result);
				if (result.contains("1"))
					userExists = true;
				else
					userExists = false;
			} catch (Exception e)
			{
				Log.e("log_tag", "Error converting result " + e.toString());
			}

			// parse json data
			try
			{
				// sresult = "{ \"logged\": 1}";

				// JSONArray jArray = new JSONArray(result);
				Log.v("test json", result);
				JSONObject jObject = new JSONObject(result);
				Log.v("test json", jObject.getString("logged"));
				System.out.println(jObject.getString("logged"));



			} catch (JSONException e)
			{
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
			return mainUI;
		}

		protected void onPostExecute(StudyMobActivity mainUI)
		{
			mainUI.updateUI();
		}

	}

}