package com.StudyMob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.StudyMob.R;
import com.StudyMob.StudyMobActivity;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class NotificationActivity extends ListActivity implements OnClickListener {
	String username;
	String password;
	String searchtext;
	String[] Display;
	int[] DisplayMOBID;
	int size = 0;
	boolean empty = false;
	int http_mobIdIndex;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.homescreen);
        SharedPreferences settings = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0);
        username = settings.getString("username","");
        password = settings.getString("password","");
        searchtext = settings.getString("searchtext","");
        
        new http_connection().execute(this);

        //setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));
    }
	
	 @Override
		protected void onResume()
		{
			super.onResume();
	        new http_connection().execute(this);
	        //setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));  
		}
	
	public void onClick(View v) {
		// TODO Auto-generated method stub
	}
	
	@Override
	 protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if(!empty) {
			Editor tokenFile = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0).edit();
			tokenFile.putInt("classID", DisplayMOBID[position]);
			tokenFile.commit();
			
			
			this.http_mobIdIndex = position;
			new http_push().execute(this);
			//http_push(position);
			Intent i = new Intent(this, ViewGroupActivity.class);
			startActivity(i);
		}
	 }
	
	
	
	
	
	/*
	void http_push(int mobIdIndex)
	{
        HttpClient httpclient1 = new DefaultHttpClient();
        HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/deleteinvite.php");
        //InputStream is = null;
        //String result = "";
        try {
        	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
        	nameValuePairs.add(new BasicNameValuePair("op","andlogin"));
        	nameValuePairs.add(new BasicNameValuePair("email", username));
        	nameValuePairs.add(new BasicNameValuePair("password", password));
        	nameValuePairs.add(new BasicNameValuePair("mid",Integer.toString(DisplayMOBID[mobIdIndex])));
        	httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        
        	httpclient1.execute(httppost1);
        	//HttpEntity entity = response.getEntity();
            //is = entity.getContent();
        }
    	catch (ClientProtocolException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	} 
    	catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
	}
	
	
	
	
	*/
	
	
	
	void updateUI()
	{
		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));  
	}

	
	
	
	
	


	private class http_connection extends
			AsyncTask<NotificationActivity, Void, NotificationActivity>
	{

		@Override
		protected void onPreExecute()
		{

		}

		protected NotificationActivity doInBackground(NotificationActivity... params)
		{
			HttpClient httpclient1 = new DefaultHttpClient();
	        HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/getinvites.php");
	        InputStream is = null;
	        String result = "";
	        try {
	        	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
	        	nameValuePairs.add(new BasicNameValuePair("op","andlogin"));
	        	nameValuePairs.add(new BasicNameValuePair("email", username));
	        	nameValuePairs.add(new BasicNameValuePair("password", password));
	        	httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
	        	HttpResponse response = httpclient1.execute(httppost1);
	        	HttpEntity entity = response.getEntity();
	            is = entity.getContent();
	            
	        	} 
	        	catch (ClientProtocolException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        	} 
	        	catch (IOException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        	}
	        	
	        	StringBuilder sb = new StringBuilder();
	        	//convert response to string
	        	try{
	        	    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	        	    String line = null;
	        	    while ((line = reader.readLine()) != null) {
	        	        sb.append(line + "\n");
	        	    }
	        	    is.close();
	        	    result=sb.toString();
	        	}catch(Exception e){
	        	    Log.e("Connecting", "Error converting result "+e.toString());
	        	}

	        	//parse json data
	        	try{

	        		Log.v("homescreen", result);
	        		JSONArray jArray = new JSONArray(result);
	        		
	                Display = new String[jArray.length()];
	                DisplayMOBID = new int [jArray.length()];
	                
	        		for(int i = 0 ; i < jArray.length() ; i++)
	        		{

	        			JSONObject j = jArray.getJSONObject(i);
	        	
	        			//"mobid" "classtitle" "start" "ouser" "desc" "loc"
	        			if(j.getInt("INVITES") == 1) {
	        				empty = false;
	        				DisplayMOBID[i] = j.getInt("mobid");
	        				Display[i] = j.getString("classtitle");
	        				Display[i] += " TIME: ";
	        				Display[i] += j.getString("start");
	        				Display[i] += " INVITER: ";
	        				Display[i] += j.getString("inviter");
	        				//j.getString("desc");
	        				//j.getString("loc");
	        			}
	        			else {
	        				Display[i] = "No New Notifcations";
	        				empty = true;
	        			}
	        		}
	        		
	        		//System.out.println(jObject.getString("logged"));

	        	}
	        	catch(JSONException e){
	        	   Log.e("Parsing", "Error parsing data "+e.toString());
	        	}
			return params[0];
		}

		protected void onPostExecute(NotificationActivity result)
		{
			result.updateUI();
		}

	}

	private class http_push extends
			AsyncTask<NotificationActivity, Void, NotificationActivity>
	{

		@Override
		protected void onPreExecute()
		{

		}

		protected NotificationActivity doInBackground(NotificationActivity... params)
		{
			NotificationActivity mainUI = params[0];
			
			 HttpClient httpclient1 = new DefaultHttpClient();
		        HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/deleteinvite.php");
		        //InputStream is = null;
		        //String result = "";
		        try {
		        	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
		        	nameValuePairs.add(new BasicNameValuePair("op","andlogin"));
		        	nameValuePairs.add(new BasicNameValuePair("email", username));
		        	nameValuePairs.add(new BasicNameValuePair("password", password));
		        	nameValuePairs.add(new BasicNameValuePair("mid",Integer.toString(DisplayMOBID[mainUI.http_mobIdIndex])));
		        	httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        
		        	httpclient1.execute(httppost1);
		        	//HttpEntity entity = response.getEntity();
		            //is = entity.getContent();
		        }
		    	catch (ClientProtocolException e) {
		    		// TODO Auto-generated catch block
		    		e.printStackTrace();
		    	} 
		    	catch (IOException e) {
		    		// TODO Auto-generated catch block
		    		e.printStackTrace();
		    	}
			return params[0];
		}

		protected void onPostExecute(NotificationActivity result)
		{
			result.updateUI();
		}

	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}