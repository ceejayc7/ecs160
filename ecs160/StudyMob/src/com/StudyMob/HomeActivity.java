package com.StudyMob;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewDebug.IntToString;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;




public class HomeActivity extends ListActivity implements OnClickListener {
	public static final String TOKEN_PREFS = "tokens";
	Button search;
	Button notifications;
	Button classes;
	Button create;
	String username;
	String password;
	String[] Display;
	int[] DisplayIDS;
	
	
	public void onCreate(Bundle savedInstanceState) {
		Log.v("HomeActivity", "HomeActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
	    
        SharedPreferences settings = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0);
        username = settings.getString("username","");
        password = settings.getString("password","");
        
        search = (Button)findViewById( R.id.search );
        search.setOnClickListener(this);
        notifications = (Button)findViewById( R.id.notifications );
        notifications.setOnClickListener(this);
        classes = (Button)findViewById( R.id.classes );
        classes.setOnClickListener(this);
        create = (Button)findViewById( R.id.create );
        create.setOnClickListener(this);
        
        //Display = new String[] {"ECS 160"};
        //ArrayList<String> passing = new ArrayList<String>();
        //passing.add("test");
        //passing.add("test12");
        
        new home_activity_http().execute(this); //no need to pass in result list
        
        
        //http_connection();
        //Display = ""
        Display = new String[0];
        //Display[0] = "";
        
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));
        
    }
	
	public void onClick(View v)
    {
    	if(v.equals(findViewById(R.id.search)))
    	{	
			EditText pin1 = (EditText) findViewById(R.id.searchtext);
			String searchtext = pin1.getText().toString().trim();
			pin1.setText("");
			Editor tokenFile = getSharedPreferences(TOKEN_PREFS,0).edit();
            tokenFile.putString("searchtext", searchtext);
			tokenFile.commit();
			Intent i = new Intent(this, SearchActivity.class);
			startActivity(i);
    	}
    	if(v.equals(findViewById(R.id.notifications)))
    	{	
			Intent i = new Intent(this, NotificationActivity.class);
			startActivity(i);
    	}
    	if(v.equals(findViewById(R.id.classes)))
    	{	
			Intent i = new Intent(this, homescreen.class);
			startActivity(i);
    	}
    	if(v.equals(findViewById(R.id.create)))
    	{	
			Intent i = new Intent(this, CreateGroupActivity.class);
			startActivity(i);
    	}
    }
	
	protected void updateUI()
	{
		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));  
	}
	
	
	 @Override
	 protected void onListItemClick(ListView l, View v, int position, long id) {
		 super.onListItemClick(l, v, position, id);
		 System.out.println("test on click listener");
		Editor tokenFile = getSharedPreferences(StudyMobActivity.TOKEN_PREFS,0).edit();
	    tokenFile.putInt("classID", DisplayIDS[position]);
	    tokenFile.commit();
		Intent i = new Intent(this, ViewGroupActivity.class);
		startActivity(i);
	 }
	 @Override
	protected void onResume()
	{
		super.onResume();
		
        //http_connection();
		ArrayList<String> passing = new ArrayList<String>();
		new home_activity_http().execute(this);
		Display = new String[0];
	    //Display[0] = "";
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));  
	}
	
	 
      
	 

	 
	 
	 private class home_activity_http extends
		AsyncTask<HomeActivity, Void, HomeActivity>
{
			String[] TempDisplay;
			int[] TempDisplayIDS;


	@Override
	protected void onPreExecute()
	{

	}

	protected HomeActivity doInBackground( HomeActivity... params)
	{
	

		HttpClient httpclient1 = new DefaultHttpClient();
        HttpPost httppost1 = new HttpPost("http://www.studymob.comuv.com/getmobs.php");
        InputStream is = null;
        String result = "";
        try {
        	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
        	nameValuePairs.add(new BasicNameValuePair("op","andlogin"));
        	nameValuePairs.add(new BasicNameValuePair("email", username));
        	nameValuePairs.add(new BasicNameValuePair("password", password));
        	httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        
        	HttpResponse response = httpclient1.execute(httppost1);
        	HttpEntity entity = response.getEntity();
            is = entity.getContent();
            
        	} 
        	catch (ClientProtocolException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	} 
        	catch (IOException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
        	
        	StringBuilder sb = new StringBuilder();
        	//convert response to string
        	try{
        	    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));/*"iso-8859-1"),8);*/
        	    String line = null;
        	    while ((line = reader.readLine()) != null) {
        	        sb.append(line + "\n");
        	    }
        	    is.close();
        	    result=sb.toString();
        	}catch(Exception e){
        	    Log.e("Connecting", "Error converting result "+e.toString());
        	}

        	//parse json data
        	try{
        		/*
        		JSONObject jobject = new JSONObject(result);
                courseString = jobject.getString("classtitle");
                timeString = jobject.getString("start");
                descriptionString = jobject.getString("desc");
                locationString = jobject.getString("loc");
                */

        		JSONArray jArray = new JSONArray(result);
        		TempDisplay = new String[jArray.length()];
        		TempDisplayIDS = new int[jArray.length()];
        		for(int i = 0 ; i < jArray.length() ; i++)
        		{
        			/*
        			 * classid  dbcid
        			 * title  dbtitle
        			 * status  cstat  // what you can provide in this 
        			 */
        			
        			Log.v("jarrray home activity","RESULT: " + result);
        			JSONObject jObject = jArray.getJSONObject(i);
        			Log.v("jarrray home activity","this is just a test");
        			TempDisplay[i] = jObject.getString("coursename");
        			TempDisplayIDS[i] = jObject.getInt("mid");

        		}
        	
        	}
        	catch(JSONException e){
        	   Log.e("Parsing", "Error parsing data "+e.toString());
        	}
        	
        	
        	
			return params[0];
	}

	protected void onPostExecute(HomeActivity result)
	{
		result.Display = this.TempDisplay;//new String[TempDisplay.length];
		result.DisplayIDS = this.TempDisplayIDS;//new String[TempDisplayIDS.length];
		result.updateUI();
		//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Display));  
		//HomeActivity
		
	}


}
	 
	 
	 
	 
}


